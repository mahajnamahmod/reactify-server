'use strict'

var AWS = require('aws-sdk');
var dynamodoc;
if(process.env.IS_OFFLINE){
	dynamodoc = new AWS.DynamoDB.DocumentClient({
		region: 'eu-west-1'
	});
}else{
	dynamodoc = new AWS.DynamoDB.DocumentClient();
}

var isEqual = require('lodash').isEqual;

import { doWhilst, each, parallel } from 'async';

/**
 * @params: {
 * 	table_name:{
 * 		Keys: [{
 * 			partition_ket: val,
 * 			sort_key?: val
 * 		}],
 * 		AttributesToGet?: [string],
 * 		ConsistentRead? : boolean
 * 		ProjectionExpression?: string
 *		ExpressionAttributeNames?: {key: val}
 * 	}
 * }
 * 
 * @return Promise
 * res(data) -> data: {
 * 	table_name: [Items]
 * }
 */
export function batchGet(params) {
	return new Promise((res, rej) => {
		let paramsChuncks = [];
		let num = 0;
		let requestParams = {};
		for(let table in params){
			for(let key of params[table].Keys){
				if(!requestParams[table]) requestParams[table] = { Keys: []};
				requestParams[table].Keys.push(key);
				num++;
				if(num === 100){
					paramsChuncks.push(requestParams);
					num = 0;
					requestParams = {};
				}
			}
		}

		if(num !== 100 && JSON.stringify(requestParams) !== JSON.stringify({})){
			paramsChuncks.push(requestParams);
		}

		let parallelBatch = [];
		paramsChuncks.forEach(params => {
			parallelBatch.push(cb => {
				doBatchReadLimit100(params).then(data => {
					cb(null, data);
				}, err => {
					cb(err);
				});
			});
		});

		parallel(parallelBatch, (err, data) => {
			if(err) return rej(err);

			let toReturn = {};
			//iterate over all datas from parallel functions
			for(let d of data){
				//iterate over each table in data returned from one function
				for(let table in d){
					if(!toReturn[table]) toReturn[table] = d[table];
					else toReturn[table] = toReturn[table].concat(d[table]);
				}
			}
			res(toReturn);

		});
	});
}

var doBatchReadLimit100 = (params) => {
	return new Promise((res, rej) => {
		let data = {};
		let currentRetry = 0;
		doWhilst((callback) => {
			let timeout = currentRetry !== 0 ? Math.pow(2, currentRetry) * 50 : 0;
			setTimeout(() => {

				dynamodoc.batchGet({ RequestItems: params }, (err, bdata) => {
					if (err) return callback(err);

					for (let table in bdata.Responses) {
						if (!data[table]) data[table] = [];
						data[table] = data[table].concat(bdata.Responses[table]);
					}

					params = bdata;

					currentRetry++;

					callback(null);

				}, timeout);

			});
		}, () => {
			//condition
			if ('UnprocessedKeys' in params && JSON.stringify(params.UnprocessedKeys) != JSON.stringify({})) {
				params = params.UnprocessedKeys;
				return true;
			} else return false;

		}, (err) => {
			if (err) return rej(err);
			return res(data);
		});
	});
};

/**
 * @params: {
 * 	table_name:[
 * 		{
 * 			DeleteRequest:{ Key: {} }
 * 		},
 * 		{
 * 			PutRequest:{ Item: {} }
 * 		}
 * 	]
 * }
 */
export function batchWrite(params) {
	return new Promise((res, rej) => {

		let paramsChuncks = [];

		let num = 0;
		let requestParams = {};
		//iterate over each table
		for(let table in params){
			//iterate over each request in table
			for(let req of params[table]){
				if(!requestParams[table]) requestParams[table] = [];
				requestParams[table].push(req);
				num++;
				if(num === 25){
					paramsChuncks.push(requestParams);
					num = 0;
					requestParams = {};
				}
			}
		}
		if(num !== 25 && JSON.stringify(requestParams) !== JSON.stringify({})){
			paramsChuncks.push(requestParams);
		}

		each(paramsChuncks, (param, cb) => {
			doBatchWriteLimit25(param).then(() => cb(), err => cb(err));
		},(err) => {
			if(err) rej(err);
			else res();
		});

	});

}

var doBatchWriteLimit25 = (params) => {
	return new Promise((res, rej) => {
		let currentRetry = 0;
		doWhilst((callback) => {
			let timeout = currentRetry !== 0 ? Math.pow(2, currentRetry) * 50 : 0;
			setTimeout(() => {

				dynamodoc.batchWrite({ RequestItems: params }, (err, bdata) => {
					if (err) return callback(err);

					params = bdata;
					currentRetry++;
					callback(null);

				}, timeout);

			});
		}, () => {
			//condition function
			if ('UnprocessedItems' in params && JSON.stringify(params.UnprocessedItems) != JSON.stringify({})) {

				params = params.UnprocessedItems;
				return true;

			} else {
				return false;
			}
				}, (err) => {
					if(err) return rej(err);
					res();
				});
	});

};

export function isKeyThere(key, keys) {
  let isThere = false;
  for (let i = 0; i < keys.length && !isThere; i++) {
    isThere = isEqual(key, keys[i]);
  }
  return isThere;
}