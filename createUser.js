"use strict";

var AWS = require("aws-sdk");
var dynamo;
if (process.env.IS_OFFLINE) {
  dynamo = new AWS.DynamoDB.DocumentClient({
    region: "localhost",
    endpoint: "http://localhost:8000"
  });
} else {
  dynamo = new AWS.DynamoDB.DocumentClient();
}
var uniqid = require("uniqid");

module.exports.main = (event, context, callback) => {
	if (event.body != undefined) {
    // let body = JSON.parse(event.body);
    var body = JSON.parse(event.body);
    // console.log(body.username);
    // let username = body['username'];
    let params = {
      TableName: "userTable",
      Item: {
        userid: uniqid(),
        username: body.username,
        password: body.password,
        img: body.link,
        reactions: []
      }
    };
	let keyGet = {
		username: body.username
	};
    const res = {
    	statusCode: 200,
    	body: JSON.stringify(params)
    }
    const responseTrue = {
      statusCode: 200,
      body: JSON.stringify({ create: "true" })
    };
	const usernameExists = {
      statusCode: 409,
      body: JSON.stringify({ create: false, usernameExists : "true" })
    };

    const responseFalse = {
      statusCode: 503,
      body: JSON.stringify({ create: "false" })
    };
    let prop = {
      TableName: "userTable",
      Key: keyGet
    };
    dynamo.get(prop, (err, data) => {
      if (err) {
        console.error(
          "error while getting user from db with username:",
          params.Item.username,
          " with error:",
          err
        );
        callback(
          "error while getting user from db with username:" +
            params.Item.username +
            " with error:" +
            err,
          null
        );
        callback(null, responseFalse);
        return;
      }
      let user = data.Item;
      if (user === undefined) {
        dynamo.put(params, (err, data2) => {
          if (err) {
            console.error(
              "error while putting to db with params:",
              params,
              "with error:",
              err
            );
            callback(
              "error while putting to db with params: " +
                params +
                " with error: " +
                err
            );
			    callback(null, responseFalse);
            return;
          } else {
            const response = {
              statusCode: 200,
              body: JSON.stringify({
                statusCode: 200,
				        create: 'true',
                username: params.Item.username
              })
            };
            callback(null, response);
            return;
          }
        });
      } else {
        callback(null, usernameExists);
        return;
      }
    });
  } else {
    const res = {
      statusCode: 503,
      body: JSON.stringify({
        usernameExists : "true",
      "reason":"wrong body"
    })
    };
    callback(null, res);
  }
};
