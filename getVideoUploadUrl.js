'use strict';
var AWS = require('aws-sdk');
var s3
if (process.env.IS_OFFLINE) {
	s3 = new AWS.S3({
		region: 'us-east-1'
	});
} else {
	s3 = new AWS.S3();
}
var uniqid = require('uniqid');
var sendVideo = require('./sendVideo').main
/*
 * Called from login.main
 * Input: JSON of (string "username", int user_location_1,int user_location_2. stirng picLink )
 * Output: JSON.stringify({ unicId: country })
 */
module.exports.main = function (event, context, callback) {

	let username = event.headers.username;	
	if(!username) username = "zeema"

	var url = s3.getSignedUrl('putObject', {
		Bucket: `upload-server-reactify2`,
		Key: uniqid() + '.mp4'
	});
	var thumbUrl = s3.getSignedUrl('putObject', {
		Bucket: `upload-server-reactify2`,
		Key: uniqid() + '.jpeg'
	});
	const response = {
		statusCode: 200,
		body: JSON.stringify({
			url,
			thumbUrl
		}),
	}
	let link = url.split("?")[0]
	thumbUrl = thumbUrl.split("?")[0]
	let e = {}
	e.body = JSON.stringify({
		username,
		link,
		thumbUrl
	})
	sendVideo(e, null, (err, data) => {
		callback(err, response)
	})
};