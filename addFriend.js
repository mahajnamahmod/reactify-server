"use strict";

var AWS = require("aws-sdk");
var dynamo;
if (process.env.IS_OFFLINE) {
  dynamo = new AWS.DynamoDB.DocumentClient({
    region: "localhost",
    endpoint: "http://localhost:8000"
  });
} else {
  dynamo = new AWS.DynamoDB.DocumentClient();
}
module.exports.main = (event, context, callback) => {
  if (event.body != undefined) {
    // let body = JSON.parse(event.body);
    var body = JSON.parse(event.body);
    // console.log(body.username);
    // let username = body['username'];
    let user1 = {
      TableName: "friendsTable",
      Item: {
        user1: body.user1,
        user2: body.user2
      }
    };
    let user2 = {
      TableName: "friendsTable",
      Item: {
        user1: body.user2,
        user2: body.user1
      }
    };
    const responseTrue = {
      statusCode: 200,
      body: JSON.stringify({ create: "true" })
    };
    const usernameExists = {
      statusCode: 409,
      body: JSON.stringify({ create: false, usernameExists: "true" })
    };

    const responseFalse = {
      statusCode: 503,
      body: JSON.stringify({ create: "false" })
    };

    dynamo.put(user1, (err, data2) => {
      if (err) {
        console.error(
          "error while putting to db with params:",
          user1,
          "with error:",
          err
        );
        callback(
          "error while putting to db with params: " +
            user1 +
            " with error: " +
            err
        );
        callback(null, responseFalse);
        return;
      } else {
        dynamo.put(user2, (err, data2) => {
            if (err) {
              console.error(
                "error while putting to db with params:",
                user2,
                "with error:",
                err
              );
              callback(
                "error while putting to db with params: " +
                  user2 +
                  " with error: " +
                  err
              );
              callback(null, responseFalse);
              return;
            } else {
              const response = {
                statusCode: 200,
                body: JSON.stringify({
                  create: "true",
                  user1: user1.Item.user1,
                  user2: user1.Item.user2
                })
              };
              callback(null, response);
              return;
            }
          });
      }

    });
  }
};
