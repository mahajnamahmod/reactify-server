'use strict';
var sendVideo = require('./sendVideo').main
var each = require('async').each
var sendVideo = require('./sendVideo').main
/*
 * Called from login.main
 * Input: JSON of (string "username", int user_location_1,int user_location_2. stirng picLink )
 * Output: JSON.stringify({ unicId: country })
 */
module.exports.main = function (event, context, callback) {

	let username = "student"
	let videosUrls = [
		"https://res.cloudinary.com/glamhuji/video/upload/c_scale,h_1920,q_50/v1495144853/IMG_1063_emr6wb.mov",
		"https://res.cloudinary.com/glamhuji/video/upload/c_scale,h_1920,q_50/v1495144861/IMG_1073_ln0owk.mov",
		"https://res.cloudinary.com/glamhuji/video/upload/c_scale,h_1920,q_50/v1495144856/IMG_1070_k6nqsy.mov",
		"https://res.cloudinary.com/glamhuji/video/upload/q_50/v1495144860/IMG_1071_wt3vt0.mov",
		"https://res.cloudinary.com/glamhuji/video/upload/q_50/v1495144856/IMG_1067_vym91r.mov",
		"https://res.cloudinary.com/glamhuji/video/upload/q_50/v1495144856/IMG_1069_az1vqa.mov",
		"https://res.cloudinary.com/glamhuji/video/upload/q_50/v1495144853/IMG_1059_tb5drg.mov",
		"https://res.cloudinary.com/glamhuji/video/upload/q_50/v1495144851/IMG_1060_qbjy6b.mov",
		"https://res.cloudinary.com/glamhuji/video/upload/q_50/v1495144851/IMG_1066_ddnwh4.mov",
		"https://res.cloudinary.com/glamhuji/video/upload/q_50/v1495144851/IMG_1061_ximu7d.mov",
		"https://res.cloudinary.com/glamhuji/video/upload/q_50/v1495144851/IMG_1068_nb5abz.mov",
		"https://res.cloudinary.com/glamhuji/video/upload/q_50/v1495144847/IMG_1065_hndnnq.mov",
		"https://res.cloudinary.com/glamhuji/video/upload/q_50/v1495144842/IMG_1062_xlovn8.mov",
		"https://res.cloudinary.com/glamhuji/video/upload/q_50/v1495144843/IMG_1064_ibpkny.mov",
		"https://res.cloudinary.com/glamhuji/video/upload/q_50/v1495144856/IMG_1067_vym91r.mov",
		"https://res.cloudinary.com/glamhuji/video/upload/q_50/v1495155991/IMG_1084_lxsrbq.mov",
		"https://res.cloudinary.com/glamhuji/video/upload/q_50/v1495155988/IMG_1085_dwnyz4.mov",
		"https://res.cloudinary.com/glamhuji/video/upload/q_50/v1495155989/IMG_1087_n7a01v.mov",
		"https://res.cloudinary.com/glamhuji/video/upload/q_50/v1495155988/IMG_1083_boqwbm.mov",
		"https://res.cloudinary.com/glamhuji/video/upload/q_50/v1495155984/IMG_1088_rgctpy.mov",
		"https://res.cloudinary.com/glamhuji/video/upload/q_50/v1495155983/IMG_1086_y5ois5.mov"
	]
	each(videosUrls, (link, cb) => {
		let e = {}
		e.body = JSON.stringify({
			username,
			link
		})
		sendVideo(e, null, cb)
	}, (error, data) => {
		const response = {
			statusCode: 200,
			body: JSON.stringify(error),
		}
		callback(null, response)
	})
};