'use strict';

var AWS = require('aws-sdk');
var dynamo
if (process.env.IS_OFFLINE) {
    dynamo = new AWS.DynamoDB.DocumentClient({
        region: 'localhost',
        endpoint: 'http://localhost:8000'
    });
} else {
    dynamo = new AWS.DynamoDB.DocumentClient();
}
module.exports.main = (event, context, callback) => {

    let params = {
        TableName: "videoTable",
        Count : true
    };
    let items = [];
    dynamo.scan(params, (err, result) => {
        if (err) {
            console.error("error while scanning db with params:", params, 'with error:', err)
            callback("error while scanning db with params: " + params + ' with error: ' + err);
        } else {
            
            const response = {
                            statusCode: 200,
                            body: result.Count
                            };
            callback(null, response);
        }
    });
            
};
