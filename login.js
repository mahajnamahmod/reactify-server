"use strict";
var AWS = require("aws-sdk");
var dynamo;
if (process.env.IS_OFFLINE) {
  dynamo = new AWS.DynamoDB.DocumentClient({
    region: "localhost",
    endpoint: "http://localhost:8000"
  });
} else {
  dynamo = new AWS.DynamoDB.DocumentClient();
}
var request = require("request");

/*
 * Called from login.main
 * Input: JSON of (string "username", int user_location_1,int user_location_2. stirng picLink )
 * Output: JSON.stringify({ unicId: country })
 */
module.exports.main = function(event, context, callback) {
  let body = JSON.parse(event.body);
  let username = body.username.toLowerCase();
  let password = body.password;

  let keyGet = {
    username: username
  };
  let prop = {
    TableName: "userTable",
    Key: keyGet
  };
  console.log(prop);
  dynamo.get(prop, (err, data) => {
    if (err) {
      console.error(
        "error while getting user from db with username:",
        params.Item.username,
        " with error:",
        err
      );
      const response = {
        statusCode: 409,
        body: JSON.stringify({
          statusCode: 409,
          body: JSON.stringify({ status: "not found" })
        })
      };
      callback(null, response);
      return;
    }
    let user = data.Item;
    if (user !== undefined) {
      if (user.password === password) {
        const response = {
          statusCode: 200,
          body: JSON.stringify({
            statusCode: 200,
            body: JSON.stringify({ status: "ok" })
          })
        };
        callback(null, response);
        return;
      } else {
        const response = {
          statusCode: 422,
          body: JSON.stringify({
            statusCode: 422,
            body: JSON.stringify({ status: "wrong username/password" })
          })
        };
        callback(null, response);
        return;
      }
    } else {
      const response = {
        statusCode: 409,
        body: JSON.stringify({
          statusCode: 409,
          body: JSON.stringify({ status: "not found" })
        })
      };
      callback(null, response);
      return;
    }
  });
  // let location1 = body.lang;
  // let location2 = body.long;
  // let stringLoc = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' + location1 + ',' + location2 + '&sensor=false'
  // request(stringLoc, (error, res, data) => {
  //     if (error) {
  //         console.error('error while requesting geo from google:', error)
  //         callback('error while requesting geo from google:' + error, null)
  //     } else {
  //         let data = JSON.parse(res.body);
  //         let country = 'Undefined';
  //         if (data.results[0] && data.results[0].address_components) {
  //             country = data.results[0].address_components.find(address => {
  //                 return address.types && Array.isArray(address.types) && address.types.indexOf('country') != -1
  //             }).long_name
  //         }
  //     }
  // })
};
