'use strict';

var AWS = require('aws-sdk');
var dynamo
if (process.env.IS_OFFLINE) {
    dynamo = new AWS.DynamoDB.DocumentClient({
        region: 'localhost',
        endpoint: 'http://localhost:8000'
    });
} else {
    dynamo = new AWS.DynamoDB.DocumentClient();
}

/*
* Called from validateUsername.main
* Input: JSON of (string "username");
* Output: JSON.stringify({ "validate": "true" }) iff name is valis; else: JSON.stringify({ "validate": "false" })
*/
module.exports.main = (event, context, callback) => {
    let username = JSON.parse(event.body).username;

    const responseTrue = {
        statusCode: 200,
        body: JSON.stringify({ "validate": "true" })
    }
    const responseFalse = {
        statusCode: 200,
        body: JSON.stringify({ "validate": "false" })
    }
    
    let prop = {
        TableName: 'userTable',
        Key: {
            username
        }
    }

    dynamo.get(prop, (err, data) => {
        if(err){
            console.error('error while getting user from db with username:', username, ' with error:', err)
            callback('error while getting user from db with username:' + username + ' with error:' + err, null)
        }
        let user = data.Item
        if (user === null) {
            callback(null, responseTrue);
        } else {
            callback(null, responseFalse);
        }
    })
};