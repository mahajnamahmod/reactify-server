"use strict";

var AWS = require("aws-sdk");
var async = require("async");
var dynamo;
var getReactions = require('./getMyReactions').main
if (process.env.IS_OFFLINE) {
  dynamo = new AWS.DynamoDB.DocumentClient({
    region: "localhost",
    endpoint: "http://localhost:8000"
  });
} else {
  dynamo = new AWS.DynamoDB.DocumentClient();
}

module.exports.main = (event, context, callback) => {
  let body = JSON.parse(event.body);
  let items = [];
  let username = body["username"];
  let paramsFriends = {
    TableName: "friendsTable",
    FilterExpression: "user1 =  :user",
    ExpressionAttributeValues: {
      ":user": username
    }
  };
  dynamo.scan(paramsFriends, (err, friendData) => {
    if (err) {
      console.error(
        "error while getting user from db with username:",
        body.username,
        " with error:",
        err
      );
      callback(null, usernameExistsFalse);
      return;
    }
    let friendArr = [];
    for (let friend of friendData.Items) {
      friendArr.push(friend.user2);
    }
    async.each(
      friendArr,
      (friend, callback) => {
        let params = {
          TableName: "videoTable",
          FilterExpression:
            "(#username = (:friend)) AND (NOT contains(#viewed, :user))",
          ExpressionAttributeNames: {
            "#viewed": "viewedBy",
            "#username": "username"
          },
          ExpressionAttributeValues: {
            ":user": username,
            ":friend": friend
          }
        };
        dynamo.scan(params, (err, result) => {
          if (err) {
            console.error(
              "error while scanning db with params:",
              params,
              "with error:",
              err
            );
            callback();
            return;
          } else {
            if (result.Count === 0) {
              // if user has watched all videos, reset
              // (currently it reset all viewedBy for all videos)
              console.log("User Watched All Vids, reseting");
              // resetViewedBy.main(0, 0, callback);
            } else {
              items.push(result);
            }
            callback();
          }
        });
      },
      err => {
        // if any of the file processing produced an error, err would equal that error
        if (err) {
          // One of the iterations produced an error.
          // All processing will now stop.
          console.log("A file failed to process");
          const responseError = {
            statusCode: 500,
            body: JSON.stringify({ error: "error with one or more queries " })
          };
          callback(null, responseError);
        } else {
          let e = {}
          e.body = JSON.stringify({
            username
          })
          getReactions(e, null, (err, data) => {
            console.log("treactions      df sjdsf hjgdasfhjadfjs ");
            
            console.log(JSON.parse(data.body).reactions)
            const response2 = {
              statusCode: 200,
              body: JSON.stringify({ Items: items, reactions: JSON.parse(data.body).reactions})
            };
            console.log("the datyajsaksdkas jkasj adfjkshjadf sjdsf hjgdasfhjadfjs ");
            console.log(data);
            console.log(response2);
            callback(err, response2)
          })
        }
      }
    );
  });
};
