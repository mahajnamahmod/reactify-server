"use strict";

var AWS = require("aws-sdk");
var dynamo;
var async = require("async");
if (process.env.IS_OFFLINE) {
  dynamo = new AWS.DynamoDB.DocumentClient({
    region: "localhost",
    endpoint: "http://localhost:8000"
  });
} else {
  dynamo = new AWS.DynamoDB.DocumentClient();
}
module.exports.main = (event, context, callback) => {
  if (event.body != undefined) {
    var body = JSON.parse(event.body);
    const usernameExistsFalse = {
      statusCode: 409,
      body: JSON.stringify({
        statusCode: 409,
        reason: "username does not exist!"
      })
    };

    let params = {
      TableName: "friendsTable",
      FilterExpression: "user1 =  :user",
      ExpressionAttributeValues: {
        ":user": body.username
      }
    };
    dynamo.scan(params, (err, data1) => {
      if (err) {
        console.error(
          "error while getting user from db with username:",
          body.username,
          " with error:",
          err
        );
        callback(null, usernameExistsFalse);
        return;
      }
      let friendNames = [];
      let friendArr = [];
      // console.log(data1)
      for (let item of data1.Items) {
        friendNames.push(item.user2);
      }
      console.log("the array is ")
      console.log(friendArr);
      async.each(
        friendNames,
        (friendName, callback) => {
          let prop = {
            TableName: "userTable",
            Key: {
              username: friendName
            }
          };
          
          dynamo.get(prop, (err, data) => {
            if (err) {
              console.error(
                "error while getting user from db with username:",
                username,
                " with error:",
                err
              );
              callback(
                "error while getting user from db with username:" +
                  username +
                  " with error:" +
                  err,
                null
              );
            }
            console.log("weeeeeeeeeeeeeeeeee")
            console.log(data);
            friendArr.push(data.Item);
            callback();
          });
        },
        err => {
          // if any of the file processing produced an error, err would equal that error
          if (err) {
            // One of the iterations produced an error.
            // All processing will now stop.
            console.log("A file failed to process");
            const responseError = {
              statusCode: 500,
              body: JSON.stringify({ error: "error with one or more queries " })
            };
            callback(null, responseError);
          } else {
            console.log("All friends have been processed successfully");
            const responseTrue = {
              statusCode: 200,
              body: JSON.stringify({
                statusCode: 200,
                success: "true",
                data:  friendArr
              })
            };
            callback(null, responseTrue);
          }
        }
      );
    });
  }
};
