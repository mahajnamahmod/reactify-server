"use strict";

var AWS = require("aws-sdk");
var s3;

if (process.env.IS_OFFLINE) {
  s3 = new AWS.S3({
    region: "us-east-1"
  });
} else {
  s3 = new AWS.S3();
}
var uniqid = require("uniqid");
var videoSetWatched = require("./videoSetWatched").main;
var addReactionToFriend = require("./addReactionToFriend").main;
/*
 * Called from login.main
 * Input: JSON of (string "username", int user_location_1,int user_location_2. stirng picLink )
 * Output: JSON.stringify({ unicId: country })
 */
module.exports.main = function(event, context, callback) {
  let username = event.headers.username;
  let watchedVid = event.headers.watchedvid;
  let friendVidOwner = event.headers.friendvidowner;
  console.log(event);
  console.log(username)
  console.log(watchedVid)
  console.log(friendVidOwner)
  
  if (!username) username = "zeema";

  var url = s3.getSignedUrl("putObject", {
    Bucket: `upload-server-reactify2`,
    Key: uniqid() + ".mp4"
  });
  var thumbUrl = s3.getSignedUrl("putObject", {
    Bucket: `upload-server-reactify2`,
    Key: uniqid() + ".jpeg"
  });
  const response = {
    statusCode: 200,
    body: JSON.stringify({
      url,
      thumbUrl
    })
  };
  let link = url.split("?")[0];
  thumbUrl = thumbUrl.split("?")[0];
  let e = {};
  e.body = JSON.stringify({
    username: username,
    vidUrl: watchedVid
  });
  let e2 = {};
  e2.body = JSON.stringify({
    username: friendVidOwner,
    vid: {
      link: link,
      sender: username,
      thumbUrl: thumbUrl
    }
  });
console.log(e);
console.log(e2);

  videoSetWatched(e, null, (err, data) => {
    addReactionToFriend(e2, null, (err, data) => {
      callback(err, response);
    });
  });
};
