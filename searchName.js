"use strict";
var AWS = require("aws-sdk");
var dynamo;
if (process.env.IS_OFFLINE) {
  dynamo = new AWS.DynamoDB.DocumentClient({
    region: "localhost",
    endpoint: "http://localhost:8000"
  });
} else {
  dynamo = new AWS.DynamoDB.DocumentClient();
}
var request = require("request");

/*
 * Called from login.main
 * Input: JSON of (string "username", int user_location_1,int user_location_2. stirng picLink )
 * Output: JSON.stringify({ unicId: country })
 */
module.exports.main = function(event, context, callback) {
  let body = JSON.parse(event.body);
  let user = body.userQuery;
  let userCalling = body.callerName.toLowerCase();
  let params = {
    TableName: "userTable",
    FilterExpression: "begins_with(#username, :user)",
    ExpressionAttributeNames: {
      "#username": "username"
    },
    ExpressionAttributeValues: {
      ":user": user.toLowerCase()
    }
  };
  let paramsFriends = {
    TableName: "friendsTable",
    FilterExpression: "user1 =  :user",
    ExpressionAttributeValues: {
      ":user": userCalling
    }
  };
  dynamo.scan(paramsFriends, (err, friendData) => {
    if (err) {
      console.error(
        "error while getting user from db with username:",
        body.username,
        " with error:",
        err
      );
      callback(null, usernameExistsFalse);
      return;
    }
    let friendArr = [];
    for(let friend of friendData.Items){
      friendArr.push(friend.user2);
    }
    dynamo.scan(params, (err, data) => {
      if (err) {
        console.error(
          "Unable to scan the table. Error JSON:",
          JSON.stringify(err, null, 2)
        );
        const response = {
          statusCode: 409,
          body: JSON.stringify({
            statusCode: 409,
            body: JSON.stringify({ status: "not found" })
          })
        };
        callback(null, response);
        return;
      } else {
        let namesArr = [];
        for (let item of data.Items){
          if(!(friendArr.indexOf(item.username) >= 0) && (item.username != userCalling) ){
            namesArr.push(item);
          }
        }
        const response = {
          statusCode: 200,
          body: JSON.stringify( {Items: namesArr })
        };
        callback(null, response);
        return;
      }
    });
  });
};
