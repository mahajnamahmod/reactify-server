"use strict";

var AWS = require("aws-sdk");
var dynamo;
if (process.env.IS_OFFLINE) {
  dynamo = new AWS.DynamoDB.DocumentClient({
    region: "localhost",
    endpoint: "http://localhost:8000"
  });
} else {
  dynamo = new AWS.DynamoDB.DocumentClient();
}
var uniqid = require("uniqid");

module.exports.main = (event, context, callback) => {
  if (event.body != undefined) {
    // let body = JSON.parse(event.body);
    var body = JSON.parse(event.body);
    // console.log(body.username);
    // let username = body['username'];
    let keyGet = {
      username: body.username
    };
    const responseTrue = {
      statusCode: 200,
      body: JSON.stringify({ create: "true" })
    };
    const usernameExists = {
      statusCode: 409,
      body: JSON.stringify({ create: false, usernameExists: "true" })
    };

    const responseFalse = {
      statusCode: 503,
      body: JSON.stringify({ create: "false" })
    };
    let prop = {
      TableName: "userTable",
      Key: keyGet
    };
    dynamo.get(prop, (err, data) => {
      if (err) {
        console.error(
          "error while getting user from db with username:",
          prop.Item.username,
          " with error:",
          err
        );
        callback(
          "error while getting user from db with username:" +
          prop.Item.username +
            " with error:" +
            err,
          null
        );
        callback(null, responseFalse);
        return;
      }
      let user = data.Item;
      const res = {
        statusCode: 200,
        body: JSON.stringify({
          reactions: data.Item.reactions,
        })
      };
      callback(null, res);
    });
  }
};
