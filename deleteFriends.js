"use strict";

var AWS = require("aws-sdk");
var dynamo;
if (process.env.IS_OFFLINE) {
  dynamo = new AWS.DynamoDB.DocumentClient({
    region: "localhost",
    endpoint: "http://localhost:8000"
  });
} else {
  dynamo = new AWS.DynamoDB.DocumentClient();
}
var uniqid = require("uniqid");

module.exports.main = (event, context, callback) => {
  let body = JSON.parse(event.body);
  let username = body["username"];
  let friend = body["friend"];
  let firstDelete = {
    TableName: "friendsTable",
    Key: {
      user1: username,
      user2: friend
    }
  };
  let secondDelete = {
    TableName: "friendsTable",
    Key: {
      user1: friend,
      user2: username
    }
  };
  dynamo.delete(firstDelete, (err, result) => {
    if (err) {
      console.error(
        "error while scanning db with params:",
        params,
        "with error:",
        err
      );
      const responseError = {
        statusCode: 500,
        body: JSON.stringify({
          error:
            "error while scanning db with params: " +
            params +
            " with error: " +
            err
        })
      };
      callback(null, responseError);
    } else {
      dynamo.delete(secondDelete, (err, result) => {
        if (err) {
          console.error(
            "error while scanning db with params:",
            params,
            "with error:",
            err
          );
          const responseError = {
            statusCode: 500,
            body: JSON.stringify({
              error:
                "error while scanning db with params: " +
                params +
                " with error: " +
                err
            })
          };
          callback(null, responseError);
        } else {
          const response = {
            statusCode: 200,
            body: JSON.stringify({
              info: "friend deleted!"
            })
          };
          callback(null, response);
        }
      });
    }
  });
};
