"use strict";

var AWS = require("aws-sdk");
var dynamo;
var async = require("async");

if (process.env.IS_OFFLINE) {
  dynamo = new AWS.DynamoDB.DocumentClient({
    region: "localhost",
    endpoint: "http://localhost:8000"
  });
} else {
  dynamo = new AWS.DynamoDB.DocumentClient();
}
var uniqid = require("uniqid");

// module.exports.main = (event, context, callback) => {
//   if (event.body != undefined) {
//     // let body = JSON.parse(event.body);
//     var body = JSON.parse(event.body);
//     // console.log(body.username);
//     // let username = body['username'];
//     let params = {
//       TableName: "userTable",
//       Key: {
//         username: body.username
//       },
//       ConditionExpression: "attribute_exists(RecordLabel)"
//     };
//     let keyGet = {
//       username: body.username
//     };
//     const res = {
//       statusCode: 200,
//       body: JSON.stringify(params)
//     };
//     const responseTrue = {
//       statusCode: 200,
//       body: JSON.stringify({ create: "true" })
//     };
//     const usernameExists = {
//       statusCode: 409,
//       body: JSON.stringify({ create: false, usernameExists: "true" })
//     };

//     const responseFalse = {
//       statusCode: 503,
//       body: JSON.stringify({ create: "false" })
//     };
//     let paramsFriends = {
//       TableName: "friendsTable",
//       FilterExpression: "user1 =  :user",
//       ExpressionAttributeValues: {
//         ":user": userCalling
//       }
//     };
//     dynamo.scan(paramsFriends, (err, friendData) => {
//       if (err) {
//         console.error(
//           "error while getting user from db with username:",
//           body.username,
//           " with error:",
//           err
//         );
//         callback(null, usernameExistsFalse);
//         return;
//       }
//       for(let friend of friendData.Items){
//         friendArr.push(friend.user2);
//       }
//       dynamo.deleteItem(params, (err, data) => {
//         if (err) {
//           console.error(
//             "Unable to delete from the table. Error JSON:",
//             JSON.stringify(err, null, 2)
//           );
//           const response = {
//             statusCode: 409,
//             body: JSON.stringify({
//               statusCode: 409,
//               body: JSON.stringify({ status: "not found" })
//             })
//           };
//           callback(null, response);
//           return;
//         } else {
//           const response = {
//             statusCode: 200,
//             body: JSON.stringify({ Items: data })
//           };
//           callback(null, response);
//           return;
//         }
//       });
//     });

//   }
// };
module.exports.main = (event, context, callback) => {
  let body = JSON.parse(event.body);
  let items = [];
  let username = body["username"];
  let paramsFriends = {
    TableName: "friendsTable",
    FilterExpression: "user1 =  :user",
    ExpressionAttributeValues: {
      ":user": username
    }
  };
  dynamo.scan(paramsFriends, (err, friendData) => {
    if (err) {
      console.error(
        "error while getting user from db with username:",
        body.username,
        " with error:",
        err
      );
      callback(null, usernameExistsFalse);
      return;
    }
    let friendArr = [];
    for (let friend of friendData.Items) {
      friendArr.push(friend.user2);
    }
    console.log("deleteing friends ============================");
    console.log(friendArr);
    async.each(
      friendArr,
      (friend, callback) => {
        let firstDelete = {
          TableName: "friendsTable",
          Key: {
            user1: username,
            user2: friend
          }
        };
        let secondDelete = {
          TableName: "friendsTable",
          Key: {
            user1: friend,
            user2: username
          }
        };
        dynamo.delete(firstDelete, (err, result) => {
          if (err) {
            console.error(
              "error while scanning db with params:",
              params,
              "with error:",
              err
            );
            const responseError = {
              statusCode: 500,
              body: JSON.stringify({
                error:
                  "error while scanning db with params: " +
                  params +
                  " with error: " +
                  err
              })
            };
            callback(null, responseError);
          } else {
            dynamo.delete(secondDelete, (err, result) => {
              if (err) {
                console.error(
                  "error while scanning db with params:",
                  params,
                  "with error:",
                  err
                );
                const responseError = {
                  statusCode: 500,
                  body: JSON.stringify({
                    error:
                      "error while scanning db with params: " +
                      params +
                      " with error: " +
                      err
                  })
                };
                callback(null, responseError);
              } else {
                callback();
              }
            });
          }
        });
      },
      err => {
        // if any of the file processing produced an error, err would equal that error
        if (err) {
          // One of the iterations produced an error.
          // All processing will now stop.
          console.log("A file failed to process");
          const responseError = {
            statusCode: 500,
            body: JSON.stringify({ error: "error with one or more queries " })
          };
          callback(null, responseError);
        } else {
          let deleteUser = {
            TableName: "userTable",
            Key: {
              username: username
            }
          };
          dynamo.delete(deleteUser, (err, result) => {
            if (err) {
              console.error(
                "error while scanning db with params:",
                params,
                "with error:",
                err
              );
              const responseError = {
                statusCode: 500,
                body: JSON.stringify({
                  error:
                    "error while scanning db with params: " +
                    params +
                    " with error: " +
                    err
                })
              };
              callback(null, responseError);
            } else {
              const response = {
                statusCode: 200,
                body: JSON.stringify({ info: "all users deleted" })
              };
              callback(null, response);
            }
          });
        }
      }
    );
  });
};
