'use strict';

var AWS = require('aws-sdk');
var dynamo
if (process.env.IS_OFFLINE) {
    dynamo = new AWS.DynamoDB.DocumentClient({
        region: 'localhost',
        endpoint: 'http://localhost:8000'
    });
} else {
    dynamo = new AWS.DynamoDB.DocumentClient();
}
module.exports.main = (event, context, callback) => {

    let params = {
        TableName: "videoTable",
    };
    let items = [];
    dynamo.scan(params, (err, result) => {
        if (err) {
            console.error("error while scanning db with params:", params, 'with error:', err)
            callback("error while scanning db with params: " + params + ' with error: ' + err);
        } else {
            console.log(result.Items);
            for(var i=0; i<result.Items.length; i++){
                let item = result.Items[i];
                let params = { TableName: "videoTable",
                             Key: {id : item.id},
                             UpdateExpression: "SET viewedBy = :val",
                             ExpressionAttributeValues : {":val" : []}};
                dynamo.update(params, (err,result) => {console.log("update: ",err,result)});
            }
            const response = {
                            statusCode: 200,
                            body: JSON.stringify({
                                username : result.Items[0].username,
                                link : result.Items[0].link
                            })};
            callback(null, response);
        };
            
    });
};