'use strict';

var AWS = require('aws-sdk');
var dynamo
if (process.env.IS_OFFLINE) {
    dynamo = new AWS.DynamoDB.DocumentClient({
        region: 'localhost',
        endpoint: 'http://localhost:8000'
    });
} else {
    dynamo = new AWS.DynamoDB.DocumentClient();
}
var resetViewedBy = require('./resetViewedBy');

module.exports.main = (event, context, callback) => {
    let body = JSON.parse(event.body);
    let username = body['username'];

    let params = {
        TableName: "videoTable",
        FilterExpression: "NOT contains(#viewed, :user)",
        ExpressionAttributeNames: {
            "#viewed": "viewedBy"
        },
        ExpressionAttributeValues: {
            ":user": username
        },
    };
    let items = [];
    dynamo.scan(params, (err, result) => {
        if (err) {
            console.error("error while scanning db with params:", params, 'with error:', err)
            callback("error while scanning db with params: " + params + ' with error: ' + err);
        } else {
            if (result.Count === 0) {
                // if user has watched all videos, reset
                // (currently it reset all viewedBy for all videos)
                console.log("User Watched All Vids, reseting");
                resetViewedBy.main(0, 0, callback);                
                return ;
            }
            let index = parseInt("" + Math.random() * result.Items.length);
            console.log("Choose randomly video number " + index + " out of " + result.Items.length)
            let item = result.Items[index];
            item.viewedBy.push(username);
            params = {
                TableName: "videoTable",
                Item: item,
            }
            dynamo.put(params, (err, data) => {
                if (err) {
                    console.error("error while putting to db with params:", params, 'with error:', err)
                    callback("error while putting to db with params: " + params + ' with error: ' + err);
                } else {
                    const response = {
                        statusCode: 200,
                        body: JSON.stringify({
                            username: item['username'],
                            link: item['link']
                        })
                    };
                    callback(null, response);
                }
            });
        }
    });
};