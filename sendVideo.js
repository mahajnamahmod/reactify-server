'use strict';
var uniqid = require('uniqid');
var AWS = require('aws-sdk');
AWS.config = new AWS.Config();
var dynamo
if (process.env.IS_OFFLINE) {
  dynamo = new AWS.DynamoDB.DocumentClient({
    region: 'localhost',
    endpoint: 'http://localhost:8000'
  });
} else {
  dynamo = new AWS.DynamoDB.DocumentClient();
}
module.exports.main = (event, context, callback) => {
  let body = JSON.parse(event.body);
  let videoData = {
    id: body.link,
    link: body.link,
    username: body.username,
    viewedBy: [],
    thumbUrl: body.thumbUrl
  };
  let params = {
    TableName: "videoTable",
    Item: videoData
  }
  dynamo.put(params, (err, data) => {
    if (err) {
      console.error('error while putting to db with params:', params, 'with error:', err)
      callback('error while putting to db: ' + err, null)
    } else {
      const response = {
        statusCode: 200,
        body: JSON.stringify(videoData),
      }
      callback(null, response)
    }
  });
};