"use strict";

var AWS = require("aws-sdk");
var dynamo;
if (process.env.IS_OFFLINE) {
  dynamo = new AWS.DynamoDB.DocumentClient({
    region: "localhost",
    endpoint: "http://localhost:8000"
  });
} else {
  dynamo = new AWS.DynamoDB.DocumentClient();
}
module.exports.main = (event, context, callback) => {
  let body = JSON.parse(event.body);
  let username = body["username"];
  let link = body["vidUrl"];

  let params = {
    TableName: "videoTable",
    Key: {
      id: link
    }
  };
  console.log("the params in the vid");
  console.log(params);
  let items = [];
  dynamo.get(params, (err, result) => {
    if (err) {
      console.error(
        "error while scanning db with params:",
        params,
        "with error:",
        err
      );
      callback(
        "error while scanning db with params: " + params + " with error: " + err
      );
    } else {
      let viewers = result.Item.viewedBy;
      viewers.push(username);
      let params2 = {
        TableName: "videoTable",
        Key: { id: link },
        UpdateExpression: "SET viewedBy = :val",
        ExpressionAttributeValues: { ":val": viewers }
      };
      dynamo.update(params2, (err, result) => {
        if (err) {
          console.error(
            "error while scanning db with params:",
            params,
            "with error:",
            err
          );
          callback(
            "error while scanning db with params: " +
              params +
              " with error: " +
              err
          );
        } else {
          const response = {
            statusCode: 200,
            body: JSON.stringify({
              info: "updated!!"
            })
          };
          callback(null, response);
        }
      });
    }
  });
};
