"use strict";

var AWS = require("aws-sdk");
var dynamo;
if (process.env.IS_OFFLINE) {
  dynamo = new AWS.DynamoDB.DocumentClient({
    region: "localhost",
    endpoint: "http://localhost:8000"
  });
} else {
  dynamo = new AWS.DynamoDB.DocumentClient();
}
var uniqid = require("uniqid");

module.exports.main = (event, context, callback) => {
  if (event.body != undefined) {
    // let body = JSON.parse(event.body);
    var body = JSON.parse(event.body);
    // console.log(body.username);
    // let username = body['username'];
    var toDelete = body.vidToDelete;
    let keyGet = {
      username: body.username
    };
    let prop = {
      TableName: "userTable",
      Key: keyGet
    };
    dynamo.get(prop, (err, data) => {
      if (err) {
        console.error(
          "error while getting user from db with username:",
          prop.Item.username,
          " with error:",
          err
        );
        callback(
          "error while getting user from db with username:" +
            prop.Item.username +
            " with error:" +
            err,
          null
        );
        callback(null, responseFalse);
        return;
      }
      let newReactions = [];
      for (let reaction of data.Item.reactions) {
        if (reaction.link !== toDelete) {
          newReactions.push(reaction);
        }
      }
      let params = {
        TableName: "userTable",
        Key: { username: body.username },
        UpdateExpression: "SET reactions = :val",
        ExpressionAttributeValues: { ":val": newReactions }
      };
      dynamo.update(params, (err, result) => {
        if (err) {
          console.error(
            "error while scanning db with params:",
            params,
            "with error:",
            err
          );
          callback(
            "error while scanning db with params: " +
              params +
              " with error: " +
              err
          );
        } else {
          const response = {
            statusCode: 200,
            body: JSON.stringify({
              info: "removed!!!"
            })
          };
          callback(null, response);
        }
      });
    });
  }
};
